import React, { useState } from 'react';
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// COMPONENTS IMPORT 
import EventWinnerCard from "../components/mainDB/eventWinnerCard.js";
import Footer from "../components/mainDB/footer.js";
import GamesBox from "../components/mainDB/games-box.js";
import PerGame from "../components/mainDB/per-game.js";
import PerGameBox from '../components/mainDB/per-game-box';
import GamesBoxMd from '../components/mainDB/games-box-medium-srn.js';

/* IMAGES IMPORT*/
import cup from '../images/cup.png';
import award from "../images/award.png";
import trifecta from "../images/trifecta.png";
import sharks from "../images/sharks-cup.png";

const PokemonInfo = () => {
  const [showPerGame, setShowPerGame] = useState(false);
  const [showPerGameBox, setShowPerGameBox] = useState(true);

  const showPerGameOnClick = () => {
    setShowPerGame(!showPerGame);
    setShowPerGameBox(!showPerGameBox);
  };

  return (
    <>
      <EventWinnerCard />

      <div className="one-v-one-box d-md-none">
        <div className="games-container mt-3">
          <div className="logo-container">
            <div className="p-2" role="button" tabIndex={0} onClick={showPerGameOnClick}>
              <img src={cup} alt="cup" height="40" />
            </div>
            <div className="p-2" role="button" onClick={showPerGameOnClick}>
              <img src={award} alt="award" height="40" />
            </div>
            <div className="p-2">
              <img src={trifecta} alt="trifecta" height="40" />
            </div>
          </div>

          {showPerGameBox && <PerGameBox />}
          {showPerGame && <PerGame/>}
        </div>
      </div>

      {/* GAMES BOX SECTION */}
      <GamesBox />
      <GamesBoxMd/>

      <div className="d-flex mt-3">
        <h5 className="special-events-header text-light">SPECIAL EVENTS</h5>
        <p className="view-all-text">VIEW ALL</p>
      </div>

      <div className="d-flex justify-content-center">
        <img src={sharks} alt="sharks" />
      </div>

      <p className="fury-text text-light">SPECTRAL FURY</p>
      <div className="after-photo-text text-light">
        <p><span className="name-span">FEDOR GORD VS CARLO BIADO</span> JUNE 5, 2023 | RACE TO 90 | 3 DAYS</p>
      </div>

      {/* FOOTER SECTION  */}
      <Footer />
    </>
  );
};

export default PokemonInfo;
