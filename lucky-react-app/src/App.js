import './App.css';
import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import MainDB from "./pages/mainDB.js"
import Pergame from "./pages/per-game.js"
import PerGamePage from "./pages/pergame-page.js"


function App() {
  return (
   <>

    <Router>

      <Routes>

          <Route path="/DB" element={<MainDB/>}/>
          <Route path="/" element={<Pergame/>}/>
          <Route path="/pergame" element={<PerGamePage/>}/>

      </Routes>

    </Router>

   </>
  );
}

export default App;
