import "../../CSS/copy.css"
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Xbutton from "../../images/closecircle.png"
const PlaceBetBox = () => {

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

    return (
        <>
        
          <div className="container mt-3">
            <div className="row d-flex justify-content-center">
              <div className="col-12 col-md-5 d-flex">
                {/* <!-- RED CARD --> */}
                <div className="red-card col-6 bg-danger">

                  <div className="names-container1">
                    <p className="text-center">#1 DENNIS ORCOLLO</p>
                  </div>

                  {/* <!-- ODDS --> */}
                  <div className="odds-container">
                    <p><span className="odd-span">ODDS</span> 1.0X</p>
                  </div>

                  {/* <!-- PAYOUT, TOTAL BETS, PLACE BET --> */}
                  <div className="t-p-p-container1 mt-5">

                    <div className="total-bets-container1">

                      <div className="text-container">
                        <p>TOTAL BETS <span>P100,000</span></p>
                      </div>

                    </div>

                    <div className="total-bets-container1">
                      <div className="text-container">
                        <p>PAYOUT <span>P20,000</span></p>
                      </div>
                    </div>

                    <div className="total-bets-container1">

                      <div className="text-container" type="button">
                        <p>PLACED BET <span>P1,000</span></p>
                      </div>
                      
                    </div>

                  </div>

                  <div className="col-11 place-bet-container mt-2 
                  " onClick={handleShow}>

                    <p className=" text-light
                    ">PLACE BET</p>

                  </div>

                </div>
                {/* <!-- BLUE CARD --> */}
                <div className="col-6 yellow-card">

                  <div className="names-container2">
                    <p className="text-center">#2 JOHANN CHUA</p>
                  </div>

                  {/* <!-- ODDS --> */}
                  <div className="odds-container1">
                    <p><span className="odd-span">ODDS</span> 1.0X</p>
                  </div>

                  {/* <!-- PAYOUT, TOTAL BETS, PLACE BET --> */}
                  <div className="t-p-p-container mt-5">

                    <div className="total-bets-container">
                      <div className="text-container">
                        <p>TOTAL BETS <span>P100,000</span></p>
                      </div>
                    </div>

                    <div className="total-bets-container">
                      <div className="text-container">
                        <p>PAYOUT <span>P20,000</span></p>
                      </div>
                    </div>

                    <div className="total-bets-container">
                      <div className="text-container ">
                        <p>PLACED BET <span>P1,000</span></p>
                      </div>
                    </div>

                  </div>

                  <div className="col-11 place-bet-container mt-2
                  " onClick={handleShow}>

                    <p className=" text-light
                    ">PLACE BET</p>

                  </div>
                  
                </div>
              </div>
            </div>
          </div>

          <Modal show={show} onHide={handleClose}>

        <Modal.Body>

        <div className="red-bg-modal" style={{
            backgroundColor: 'red',
            background: 'linear-gradient(131deg, rgba(225,65,29,1) -25%, rgba(224,111,86,1) 84%)',
            borderRadius: '10px'
          }}>
          <div className="names-container1 d-flex align-items-center">
            <p>#1 DENNIS ORCOLLO</p>
            <button
                className="close"
                style={{
                  position: 'absolute',
                  top: '10px',
                  right: '20px',
                  color: '#fff',
                  fontSize: '24px',
                  background: 'transparent',
                  border: 'none',
                }}
                onClick={handleClose}
              >
                &times;
              </button>
          </div>
          
          <div className="button-container" style={{
              height: '40vh',
              width: '60%',
              marginLeft: '40%',
          }}>

            <div className="d-flex justify-content-center align-items-center odd-pay-out mt-4">
                {/* ODDS */}
                <div className="text-light" style={{
                  width: '20px'
                }}>
                    <p><span>ODDS</span> 1.0X</p>
                </div>

                {/* PAYOUT */}
                <div className="text-light" style={{
                  width: '20px'
                }}>
                    <p><span>PAYOUT</span> 1000</p>
                </div>

            </div>
            
            <div className="d-flex justify-content-center ">
              <div className="payout-container text-light text-center" style={{
                width: '70px'
              }}>
                <p><span>PLACED BET</span> 0.00</p>
              </div>
            </div>
            
            <div className="taya-total-container">
              <div className="text-light">
                <p>TAYA</p>
                <h6>P0.00</h6>
              </div>

              <div className="text-light d-flex">

              {/* 100 */}
              <div>
                <p>100</p>
              </div>

              {/* 300 */}
              <div>
                <p>300</p>
              </div>

              {/* 500 */}
              <div>
                <p>500</p>
              </div>

            </div>
            </div>


          </div>

        </div>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
        
        </>
    )
}

export default PlaceBetBox;