import vector1 from "../../images/vector1.png"
import vector2 from "../../images/vector2.png"

import "../../CSS/EventSelector.css"

const EventSelector = () => {


    return (

        <>
        
            <div className="container">
                <div className="row d-flex justify-content-center">

                    <div className="col-11 col-lg-7 col-md-10 bg-warning d-flex">

                        <div className="d-flex align-items-center col-2 vector1">

                            <img src={vector1} height="20"/>
                            <p className="mt-3">PREV</p>

                        </div>

                        <div className=" event-headline-text col-8">

                            <p className="">ARENA</p>
                            <h5>TIGER ARENA</h5>
                            <p>SGT. ESQUERRA</p>

                        </div>

                        <div className="d-flex align-items-center col-2 vector1">

                            <p className="mt-3">NEXT</p>
                            <img src={vector2} height="20"/>

                        </div>

                    </div>

                </div>
            </div>
        </>
    )
}

export default EventSelector;