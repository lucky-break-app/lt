import cup from '../../images/cup.png'
import award from "../../images/award.png"
import clover from "../../images/clover.png"
import "../../CSS/games-box-md-srn.css"

const gamesBox = () => {


    return (

        <>

        {/* PER GAME AND ALL PHOTO */}
        <div className="d-flex justify-content-center pergame-pick1-container mt-2 d-md-none">
            {/* per game photo container */}
            <div className="per-game-photo">
                <img src={cup} height="60" />
                <h1>PER GAME</h1>
                <p className="text-center text-light">Tumaya sa kada rack habang nag lalaban</p>
            </div>

            <div className="pick1-photo">
                <img src={award} height="60" />
                <h1>PICK-1</h1>
                <p className="pick1-text text-center text-light">Tumaya sa mananalo bago magumpisa ang laban</p>
            </div>
        </div>

            {/* NUMBER GAMES */}
        <div className="d-flex justify-content-center mt-2 d-md-none">
            <div className="d-flex number-games-container">
                <img src={clover} height="70" className="mt-4" />
                <div className="number-games-text">
                <h1 className="number-games-header number-games-headline">NUMBER GAMES</h1>
                <p className="text-light">Pumili ng 2 digit combination ng tatayaan sa 10 main Ring. <span className='ten-text'>10 pesos tama 4,000 pesos</span></p>
                </div>
            </div>
        </div>

            </>
    )
}

export default gamesBox;