import React, { useState, useEffect } from 'react';
import OneVOne from "./one-v-one.js";
import "../../CSS/per-game-box.css"


const PerGameBox = () => {
  const [pokemonData, setPokemonData] = useState([]);

  useEffect(() => {
    fetch('https://pokeapi.co/api/v2/pokemon/ditto')
      .then(res => res.json())
      .then(data => {
        console.log(data);
        console.log(data.abilities);

        const abilities = data.abilities.map(ability => ability.ability.name);
        setPokemonData(abilities);
      });
  }, []);

  return (
    <>
      {/* FIRST CURRENT PLAYING PLAYER BOX */}
      <div className="player-cards-container text-light d-md-none">
        <div className="d-flex top-heading">
          <h3 className="text-light per-game-text">PER GAME</h3>
          <p className="text-light date-played">14:00:00 JUNE 21, 2023</p>
        </div>
        <h4 className="text-light text-center tiger-arena-text">TIGER ARENA</h4>
        <OneVOne abilities={pokemonData} /> {/* Pass the abilities data as a prop */}
        <div className="d-flex justify-content-center odds-container">
          <h8>ODDS: 1.0X</h8>
          <h8>ODDS: 1.0X</h8>
        </div>
        {/* VIEW MATCH */}
        <div className="mt-1">
          <h6 className="text-center view-match-text">VIEW MATCH</h6>
        </div>
      </div>

      {/* SECOND CURRENT PLAYING PLAYER BOX */}
      <div className="d-md-none player-cards-container1 text-light">
        <div className="d-flex top-heading">
          <h3 className="text-light per-game-text">PER GAME</h3>
          <p className="text-light date-played">14:00:00 JUNE 21, 2023</p>
        </div>
        <h4 className="text-light text-center tiger-arena-text">GREAT WHITE ARENA</h4>
        <OneVOne abilities={pokemonData} /> {/* Pass the abilities data as a prop */}
        <div className="d-flex justify-content-center odds-container">
          <h8>ODDS: 1.0X</h8>
          <h8>ODDS: 1.0X</h8>
        </div>
        <div className="mt-1">
          <h6 className="view-match-text text-center">VIEW MATCH</h6>
        </div>
      </div>
    </>
  );
};

export default PerGameBox;
