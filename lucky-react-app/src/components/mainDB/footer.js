import pagcor from "../../images/pagcor.png"

const footer = () => {

    return (

        <>
        
        <div className="mt-5 pt-5 d-flex justify-content-center">

        <img src={pagcor} />

        </div>

        <div className="text-light footer-text text-center mb-3">

        <p>Licenced by PAGCOR</p>
        <p>All rights Reserved by Lucky Taya Gaming Corporation</p>
        <p>Gaming for 21 years old and above only.</p>
        <p>Keep it fun | Game responsibly</p>

        </div>
        
        </>
    )
}

export default footer