import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import OneVOne from "./one-v-one.js";

const PerGame = () => {
  const [pokemonData, setPokemonData] = useState([]);

  useEffect(() => {
    fetch('https://pokeapi.co/api/v2/pokemon/ditto')
      .then(res => res.json())
      .then(data => {
        console.log(data);
        console.log(data.abilities);

        const abilities = data.abilities.map(ability => ability.ability.name);
        setPokemonData(abilities);
      });
  }, []);

  return (
    <>
      {/* FIRST CURRENT PLAYING PLAYER BOX */}
      <div className="player-cards-container2 text-light">
        <div className="d-flex top-heading">
          <h3 className="text-light per-game-text">PICK-1</h3>
          <p className="text-light date-played">14:00:00 JUNE 21, 2023</p>
        </div>
        <div>
        <h4 className=" text-center tiger-arena-text1 font-italic">SPECTRAL FURY DAY 2</h4>
        <p className='text-center font-italic mt-0 tiger-arena-txt-pick1'>TIGER ARENA</p>
        </div>

        <div className='bg-light mb-3'>
            <h1 className='font-italic text-dark gross-txt'>GROSS : P 1,000,000.00</h1>
        </div>

        <OneVOne abilities={pokemonData} /> {/* Pass the abilities data as a prop */}
        <div className="d-flex justify-content-center odds-container3">
          <p>ODDS: 1.0X</p>
          <p>ODDS: 1.0X</p>
        </div>
        {/* VIEW MATCH */}
        <div className="mt-1">
          <h6 className="text-center view-match-text">VIEW MATCH</h6>
        </div>

        {/* BATTLE SIEGE 2 */}
        <div className='battle-siege-container py-2'>
            <h5 className='font-italic tiger-arena-text1 text-center'>BATTLE SIEGE 1 DAY 2</h5>
            <p className='text-center tiger-arena-txt-pick1'>GREAT WHITE ARENA</p>
        </div>
        <div className='bg-light'>
            <h1 className='font-italic text-dark gross-txt'>GROSS : P 1,000,000.00</h1>
        </div>

      </div>
    </>
  );
};

export default PerGame;
